CREATE TABLE products (
    product_id INT AUTO_INCREMENT,
    name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    PRIMARY KEY (product_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;



CREATE TABLE images (
    image_id INT AUTO_INCREMENT,
    product_id INT,
    image_data LONGBLOB NOT NULL,
    PRIMARY KEY (image_id),
    FOREIGN KEY (product_id) REFERENCES products(product_id) ON DELETE CASCADE
);