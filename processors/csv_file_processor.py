import ast
import os
from pathlib import Path

from domain.image_wrapper import ImageWrapper
from domain.product import Product
from constants.constants_aggregator import INPUT_FILE_PATH
from utils.utils import image_to_byte_array, get_project_root_dir
from PIL import Image
import pandas as pd


class CSVFileProcessor:
    def __init__(self):
        self._category = None

    def process(self, file_path):
        pandas_df = pd.read_csv(file_path)
        pandas_df = pandas_df.rename(columns={'Name': 'name', 'Price': 'price'})
        pandas_df = pandas_df.dropna(subset=['name', 'price', 'images'])
        self._category = file_path.split('/')[:7][6]
        return self.product_data_populate(pandas_df)

    def parse_images(self,image_data):
        images_list = []
        for item in image_data:
            file_path = f'{INPUT_FILE_PATH}/{self._category}/{item["path"]}'
            file_real_path = os.path.join(get_project_root_dir(), file_path)
            image = Image.open(file_real_path)
            image = ImageWrapper(item['url'], item['path'], item['checksum'], item['status'],
                                 image_to_byte_array(image))
            images_list.append(image)
        return images_list

    def product_data_populate(self, pandas_df: pd.DataFrame) -> [Product]:
        product_list = []
        i = 0
        for index, raw in pandas_df.iterrows():
            i = i + 1
            print("Processing DF Index: " + str(i))
            parsed_data = ast.literal_eval(raw['images'])
            image_list = self.parse_images(parsed_data)
            product = Product(i, raw['name'], raw['price'], image_list)
            product_list.append(product)
        return product_list
