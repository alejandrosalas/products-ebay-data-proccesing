
class FileDataProcessorContext:
    def __init__(self, processor):
        self._processor = processor

    def process_file(self, file_path):
        return self._processor.process(file_path)


