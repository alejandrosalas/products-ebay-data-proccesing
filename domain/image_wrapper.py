class ImageWrapper:
    def __init__(self, url, path, checksum, status, data) -> None:
        self.url = url
        self.path = path
        self.checksum = checksum
        self.status = status
        self.data = data
