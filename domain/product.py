from domain.image_wrapper import ImageWrapper


class Product:
    def __init__(self, product_id: int, name: str, price, images: [ImageWrapper]):
        self.product_id = product_id
        self.name = name
        self.price = price
        self.images = images
