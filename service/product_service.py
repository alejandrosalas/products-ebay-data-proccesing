
class ProductService:
    def __init__(self, repository):
        self._repository = repository

    def get_all_products(self):
        return self._repository.get_all()

    def get_product_by_id(self, id):
        pass

    def create(self, product):
        return self._repository.create(product)

    def delete(self, id):
        pass
