import os

from constants.constants_aggregator import INPUT_FILE_PATH
from repository.sql_repository import SQLRepository
from service.product_service import ProductService

from processors.csv_file_processor import CSVFileProcessor
from processors.file_data_processor_context import FileDataProcessorContext

product_data_categories = ['childclothes', 'menclothes', 'womanclothes', 'menshoes']

if __name__ == '__main__':
    # DB Client
    mysql_repository = SQLRepository()
    product_service = ProductService(mysql_repository)
    # print(product_service.get_all_products())

    for category_name in product_data_categories:
        file_path = f'{INPUT_FILE_PATH}/{category_name}/output.csv'
        file_real_path = os.path.join(os.path.dirname(__file__), file_path)
        csv_processor = CSVFileProcessor()
        csv_context = FileDataProcessorContext(csv_processor)
        products = csv_context.process_file(file_real_path)

        for product in products:
            product_service.create(product)
