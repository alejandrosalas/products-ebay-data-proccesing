import base64
import io
from pathlib import Path

from PIL.Image import Image


def image_to_byte_array(image: Image) -> bytes:
    # BytesIO is a file-like buffer stored in memory
    imgByteArr = io.BytesIO()
    # image.save expects a file-like as a argument
    image.save(imgByteArr, format=image.format)
    # Turn the BytesIO object back into a bytes object
    imgByteArr = imgByteArr.getvalue()
    return imgByteArr


def byte_array_to_base64(data: bytes) -> str:
    data_64 = base64.b64encode(data)
    data_64 = data_64.decode()
    print('<img src="data:image/png;base64,{}">'.format(data))
    return data_64


def open_image_and_display(data: bytes):
    file = open('output.png', 'wb')
    file.write(data)
    file.close()


def get_project_root_dir():
    return Path(__file__).absolute().parent.parent
