from repository.irepository import IRepository


class InMemoryRepository(IRepository):
    def __int__(self):
        self._data_source = []

    def get_all(self):
        return self._data_source

    def get_by_id(self, id):
        pass

    def create(self, item):
        item['id'] = len(self._data_source) + 1
        self._data_source.append(item)
        return item

    def update(self, item):
        pass

    def delete(self, id):
        pass
