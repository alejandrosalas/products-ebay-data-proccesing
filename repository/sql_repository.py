from repository.irepository import IRepository
from constants.constants_aggregator import MYSQL_HOST, MYSQL_PORT, MYSQL_USER_NAME, MYSQL_PASSWORD, MYSQL_DATABASE
from mysql.connector import connect, Error


class SQLRepository(IRepository):
    def __init__(self):
        try:
            self._connection = connect(
                host=MYSQL_HOST,
                port=MYSQL_PORT,
                user=MYSQL_USER_NAME,
                password=MYSQL_PASSWORD,
                database=MYSQL_DATABASE
            )
        except Error as e:
            print(e)

    def get_all(self):
        cursor = self._connection.cursor()
        cursor.execute("SELECT p.product_id, p.name, p.price, i.image_id, i.image_data FROM products p JOIN images i "
                       "ON p.product_id = i.product_id;")
        results = cursor.fetchall()
        return results

    def get_by_id(self, id):
        pass

    def create(self, product):
        print("Saving Product with ID=>" + str(product.product_id))
        name = product.name
        price = product.price
        s = price.replace('$', '')  # Elimina el símbolo del dólar
        price = float(s)  # Convierte la cadena a float
        _images = product.images
        cursor = self._connection.cursor()
        cursor.execute("INSERT INTO products(name, price) VALUES (%s, %s)", (name, price))
        product_id = cursor.lastrowid

        for image_byte in _images:
            cursor.execute("INSERT INTO images (product_id, image_data) VALUES (%s, %s)", (product_id, image_byte.data))
        #insert_query = "INSERT INTO images (product_id, image_data) VALUES (%s, %s)"
        #cursor.executemany(insert_query, [(product_id, image) for image in product['images']])
        self._connection.commit()
        return product

    def update(self, item):
        pass

    def delete(self, id):
        pass
