import configparser
import os

parser = configparser.SafeConfigParser()
parser.read(os.path.join(os.path.dirname(__file__), '../config/config.conf'))

### MYSQL ###
MYSQL_HOST = parser.get("mysql", "mysql_host")
MYSQL_PORT = parser.get("mysql", "mysql_port")
MYSQL_USER_NAME = parser.get("mysql", "mysql_user_name")
MYSQL_PASSWORD = parser.get("mysql", "mysql_password")
MYSQL_DATABASE = parser.get("mysql", "mysql_database")
### MYSQL ###

### FILE ###
INPUT_FILE_PATH = parser.get("file_path","input_path")
### FILE ###
